use std::net::{IpAddr, UdpSocket, SocketAddr};
use std::fs;
use std::path::{Path, PathBuf};
use std::io::{Error as ioError};
use std::time::{SystemTime, Duration};
use std::env;
use std::thread::sleep;


type Duple = (String, String);

struct RawTemp ( Vec<Duple> );

impl RawTemp {

    fn serialize(&self) -> String {
        let s: &Vec<String> = &self.0.iter().map(|(k, v)| {
            format!(r#""{}":"{}""#, k, v)
        }).collect();
        // let middle_data: String = s.join(",");
        format!["{{{}}}\n", s.join(",")]
    }

    fn add(&mut self, k: &str, v: &str) {
        self.0.push((k.to_owned(), v.to_owned()));
    }

    fn tag(&mut self, id: &str) {
        self.0.push(("id".to_owned(), id.to_owned()));
    }

    fn timestamp(&mut self) {
        self.0.push(("timestamp".to_owned(), format!["{}", timestamp()]));
    }

}


fn read_data(path: &PathBuf) -> String {
    fs::read_to_string(path).unwrap().trim().to_owned()
}


fn timestamp() -> u64 {
    match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
        Ok(n) => n.as_secs(),
        Err(_) => panic!("SystemTime before UNIX EPOCH!"),
    }
}


fn discover_labels(hwmon: &str) -> Vec<String> {

    let dir = Path::new("/sys/class/hwmon").join(hwmon);

    let paths = fs::read_dir(dir).unwrap();

    let mut labels: Vec<Duple> = paths.filter_map(|pathr| pathr.ok())
        .filter_map(|path| {
            let s = path.file_name().into_string().unwrap();
            if s.contains(&"_label") {
                Some((s.to_owned(), read_data(&path.path())))
            }
            else {
                None
            }
        })
    .collect();

    labels.sort_by(|a, b| a.0.split("_").next().unwrap().partial_cmp(b.0.split("_").next().unwrap()).unwrap());

    labels.iter().cloned().map(|d| d.1).collect()

}


fn discover_inputs(hwmon: &str) -> Vec<String> {

    let dir = Path::new("/sys/class/hwmon").join(hwmon);

    let paths = fs::read_dir(dir).unwrap();

    let mut inputs: Vec<String> = paths.filter_map(|pathr| pathr.ok())
        .filter_map(|path| {
            let s = path.file_name().into_string().unwrap();
            if s.contains(&"_input") {
                Some(format!("{}", path.path().into_os_string().into_string().unwrap()))
            }
            else {
                None
            }
        })
    .collect();

    inputs.sort_by(|a, b| a.split("_").next().unwrap().partial_cmp(b.split("_").next().unwrap()).unwrap());

    inputs
}

struct Transmitter {
    targetAddr: SocketAddr,
    sourceAddr: SocketAddr,
    pub socket: UdpSocket
}

impl Transmitter {

    pub fn new(source: &str, target: &str) -> Self {

        let (s_addr, s_port) = _split_remote_addr(source);
        let sourceAddr = SocketAddr::from((s_addr, s_port));

        let (t_addr, t_port) = _split_remote_addr(target);
        let targetAddr = SocketAddr::from((t_addr, t_port));

        let mut socket = UdpSocket::bind(sourceAddr).unwrap();
        socket.set_nonblocking(true);
        Self { targetAddr, sourceAddr, socket }
    }

    pub fn transmit(&self, data: &str) -> Result<usize, ioError> {
        let sent_bytes = self.socket.send_to(data.as_bytes(), &self.targetAddr);
        sent_bytes
    }

}


fn _split_remote_addr(full_addr: &str) -> (IpAddr, u16) {
    let addr_tuple: Vec::<&str> = full_addr.split(":").collect();
    let addr: IpAddr = addr_tuple[0].parse().unwrap();
    let port: u16 = addr_tuple[1].parse().unwrap();
    return (addr, port)
}


fn report_data(string_data: &str, source_address: &str, remote_address: &str) {
    let transmitter = Transmitter::new(source_address, remote_address);
    let sent = transmitter.transmit(&string_data);
}


struct DataFile {
    name: String,
    inputs: Vec<String>,
    labels: Vec<String>,
}

impl DataFile {

    fn read_hwmon(&self) -> RawTemp {

        let mut list = Vec::new();
        for (input, label) in self.inputs.iter().zip(&self.labels) {
            // println!("{}", input);
            let data_input = read_data(&PathBuf::from(input));
            list.push((format!("{}", label), data_input));
        }

        RawTemp(list)

    }

}




fn main () {

    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);

    let hwmons: Vec<&str> = args[1].split(",").collect();

    // "192.168.0.149:6666"
    let source_address = &args[2];

    // "192.168.0.149:34255"
    let dest_address = &args[3];

    // whatever you fancy
    let id = &args[4];

    // a number
    // let secs: u64 = args[5].parse().unwrap();

    // a number
    let milisecs: u64 = args[5].parse().unwrap();


    let data_files: Vec<DataFile> = hwmons.iter().map(|hw| {

        let name = read_data(&Path::new("/sys/class/hwmon")
            .join(hw)
            .join("name")
        );

        let inputs = discover_inputs(hw);

        let labels = {
            let labels = discover_labels(hw);
            // println!("{:?}, {:?}", labels.len(), labels);
            // println!("{:?}, {:?}", inputs.len(), inputs);
            if  labels.len() != inputs.len() {
                inputs.clone().into_iter()
                    .map(|i| {
                        i.split("/").last().unwrap().to_owned()
                    })
                    .map(|i| {
                        i.split("_").next().unwrap().to_owned()
                    }).collect()
            }
            else {
                labels
            }
        };

        DataFile { name, inputs, labels }

    }).collect();


    loop {

        for d_file in &data_files {

            let serialized_data: String = {

                let mut raw_temp = d_file.read_hwmon();
                raw_temp.add("sensor", &d_file.name);
                raw_temp.tag(id);
                raw_temp.timestamp();
                raw_temp.serialize()

            };

            report_data(&serialized_data, source_address, dest_address);

        }

        sleep(Duration::from_millis(milisecs));

    }

}
